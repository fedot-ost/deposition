'use strict';
const PLUGINS = {
    HTML: require('html-webpack-plugin'),
    EXTRACT_TEXT: require('extract-text-webpack-plugin'),
    COPY_PLUGIN: require('copy-webpack-plugin')
};
const ENTRY_POINTS = {
    client: __dirname+'/source/main.js'
};
const PRODUCTION_DIR = '/prod';
const WATCH_MODE = true;
const HTML_PAGES = {
    INDEX: new PLUGINS.HTML({
        template: 'source/index.html',
        filename: 'index.html',
        hash: true,
        inject: 'head',
        cache: false
    })
}
const STYLES = {
    BOOTSTRAP: new PLUGINS.EXTRACT_TEXT('bootstrap.css')
}
const COPY_FILES = {
    OPENPGP: new PLUGINS.COPY_PLUGIN([{from: 'source/openpgp.js', to: 'openpgp.js'}])
}
const LOADERS = {
    JS: {
        test: /\.js$/i,
        loader: 'babel',
        query: {
            presets: [ 'es2015' ],
            plugins: [
                ['transform-es2015-classes', {
                    'loose': true
                }]
            ],
            compact: false
        }
    },
    CSS: {
        test: /\.css/i,
        loader: PLUGINS.EXTRACT_TEXT.extract('style-loader', 'css-loader')
    },
    HTML: {
        test: /\.html/,
        loader: 'html'
    },
    IMAGES: {
        test: /\.(jpe?g|png|gif)$/i,
        loader: 'url?limit=1000'
    }
}
module.exports = {
    entry: ENTRY_POINTS,
    output: {
        path: __dirname + PRODUCTION_DIR,
        filename: '[name].js'
    },
    plugins : [
        HTML_PAGES.INDEX,
        STYLES.BOOTSTRAP,
        COPY_FILES.OPENPGP
    ],
    module: {
        loaders: [LOADERS.JS,LOADERS.CSS,LOADERS.HTML,LOADERS.IMAGES]
    },
    watch: WATCH_MODE
}
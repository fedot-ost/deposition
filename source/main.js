import '../node_modules/bootstrap/dist/css/bootstrap.css';
import './main.css';
const doc = document;
const UTILS = require('./utils.js').Utils;
const KEYS = require('./keys.js').Keys;
class Deposition_demo{
    constructor(){
        this.dom = {
            views: {
                main: doc.querySelector('.b-main'),
                success: doc.querySelector('.b-success'),
                error: doc.querySelector('.b-error'),
                disparity: doc.querySelector('.b-browser-disparity')
            },
            tabs: Array.prototype.slice.call(doc.querySelectorAll('.nav-tabs .nav-link')),
            tabContent: Array.prototype.slice.call(doc.querySelectorAll('.tab-pane')),
            files: {
                decrypted: doc.querySelector('.b-file-decrypted'),
                encrypted: doc.querySelector('.b-file-encrypted')
            },
            btns: {
                encrypt: doc.querySelector('.b-btn-encrypt'),
                decrypt: doc.querySelector('.b-btn-decrypt'),
                to_main: Array.prototype.slice.call(doc.querySelectorAll('.b-router-main')),
                save: doc.querySelector('.b-btn-download')
            },
            forms: {
                encrypt: doc.querySelector('.b-form-encrypt'),
                decrypt: doc.querySelector('.b-form-decrypt')
            },
            changeDecryptState: Array.prototype.slice.call(doc.querySelectorAll('.b-change-decrypt-state'))
        }
        this.file = {
            url: '',
            name: ''
        }
        this.state = {
            fileLoaded: false
        }
        this.mode = 'encrypt'; //encrypt or decrypt
        this.encryptedFileExtension = 'secret';
        this.decryptState = 'user'; //user - расшифровка производится пользовательским ключём, 'organizations' - расшифровка происходит с исп. ключей организаций
        ((typeof openpgp !='undefined')&&(window.crypto||window.msCrypto)) ? this.browserDisparity = false : this.browserDisparity = true;
        //this.browserDisparity = true;
        (!this.browserDisparity) ? this.setView('main') : this.setView('disparity');
        this.setHandlers();
    }
    setView(key){
        let section;
        for(section in this.dom.views){
            this.dom.views[section].style.display = 'none';
        }
        this.dom.views[key].style.display = 'block';
    }
    setHandlers(){
        let i,self = this;
        for (i in this.dom.files){
            this.dom.files[i].addEventListener('change',function(e){
                self.uploadFile(e);
            })
        };
        //encrypt button click
        this.dom.btns.encrypt.addEventListener('click',function(e){
            if(self.state.fileLoaded){
                self.encrypt();
                e.preventDefault();
            }
        });
        //decrypt button click
        this.dom.btns.decrypt.addEventListener('click',function(e){
            if(self.state.fileLoaded){
                self.decrypt();
                e.preventDefault();
            }
        });
        //button save encrypted/decrypted file
        this.dom.btns.save.addEventListener('click',function(e){
            self.downloadFile();
        });
        //set decryption state (user key/organizations keys)
        this.dom.changeDecryptState.forEach(function(el){
            el.addEventListener('click',function(e){
                self.decryptState = el.getAttribute('data-state');
            })
        });
        //tabs
        this.dom.tabs.forEach(function(tab){
            let targetId;
            tab.addEventListener('click',function(e){
                e.preventDefault();
                self.mode = this.getAttribute('data-section');
                if(this.className.indexOf('active')=='-1'){
                    targetId = this.getAttribute('href');
                    UTILS.removeClass('active',self.dom.tabs);
                    this.className += ' active';
                    self.resetForms();
                    UTILS.removeClass('active',self.dom.tabContent);
                    doc.querySelector(targetId).className += ' active';
                }
            });
        });
    }
    uploadFile(e){
        let
            fileLink = e.target.files[0],
            reader = new FileReader(),
            self = this;
        if(this.mode=='encrypt'){
            this.file.extension = UTILS.extension(fileLink.name);
            this.file.name = this.deleteExtension(fileLink.name) + '.' + this.encryptedFileExtension;
        }else{
            this.file.name = fileLink.name;
        }
        reader.readAsArrayBuffer(fileLink);
        this.file.type = fileLink.type;
        reader.onload = function(res){
            let arr8 = new Uint8Array(res.target.result);
            self.file.result = arr8;
            if(self.mode=='encrypt'){
                self.saveExtension();
            }
            self.state.fileLoaded = true;
        }
    }
    downloadFile(){
        let
            self = this,
            reader = new FileReader(),
            blob = new Blob([this.file.result]);
        if (navigator.appVersion.toString().indexOf('.NET')>0){
            //ie
            window.navigator.msSaveBlob(blob, this.file.name);
        }else{
            //normal browsers
            reader.readAsDataURL(blob);
            reader.onload = function(res){
                let downloadLink = doc.createElement('a');
                if(self.mode=='decrypt'){
                    let extension = self.getExtansion();
                    self.file.name = self.deleteExtension(self.file.name)+'.'+extension;
                }
                downloadLink.download = self.file.name;
                downloadLink.href = res.target.result;
                doc.body.appendChild(downloadLink);
                downloadLink.click();
            }
        }
        self.setView('main');
    }
    encrypt(){
        let
            keys = KEYS.getKeys(),
            options = [
                {
                    data : this.file.result,
                    passwords : [
                        keys.orgKey1,
                        keys.userKey
                    ],
                    armor: false
                },
                {
                    data : '',
                    passwords : [
                        keys.orgKey2,
                        keys.userKey
                    ],
                    armor: false
                }
            ],
            self = this;
        //first step: uesr key + org1
        openpgp.encrypt(options[0]).then(function (res) {
            options[1].data = res.message.packets.write();
            //second step: user key + org2
            openpgp.encrypt(options[1]).then(function (res) {
                self.file.result = res.message.packets.write();
                self.setView('success');
                self.resetForms();
            }).catch(function (err) {
                console.log('encryption error on step 1', err);
                self.setView('error');
            });
        }).catch(function (err) {
            console.log('encryption error on step 2', err);
            self.setView('error');
        });
    }
    decrypt(){
        if(this.decryptState=='user'){
            let
                ff = this.file.result,
                self = this,
                keys = KEYS.getKeys(),
                options = {
                    message: openpgp.message.read(ff),
                    password: keys.userKey,
                    format: 'binary'
                };
            openpgp.decrypt(options).then(function (res) {
                options.message = openpgp.message.read(res.data);
                openpgp.decrypt(options).then(function (res) {
                    self.file.result = res.data;
                    self.setView('success');
                    self.resetForms();
                }).catch(function(err){
                    console.log('decryption error on user state, 2', err);
                    self.setView('error');
                });
            }).catch(function (err) {
                console.log('decryption error on user state', err);
                self.setView('error');
            });
        }else{
            let
                ff = this.file.result,
                self = this,
                keys = KEYS.getKeys(),
                options = {
                    message: openpgp.message.read(ff),
                    password: keys.orgKey2,
                    format: 'binary'
                };
            openpgp.decrypt(options).then(function (res) {
                options.message = openpgp.message.read(res.data);
                options.password = keys.orgKey1;
                openpgp.decrypt(options).then(function (res) {
                    self.file.result = res.data;
                    self.setView('success');
                    self.resetForms();
                }).catch(function(){
                    console.log('decryption error on org state, 2', err);
                    self.setView('error');
                });
            }).catch(function (err) {
                console.log('decryption error on org state, 1', err);
                self.setView('error');
            });
        }
    }
    resetForms(){
        this.dom.forms.encrypt.reset();
        this.dom.forms.decrypt.reset();
    }
    saveExtension(){
        let
            file = UTILS.arr8toStr(this.file.result),
            hash = UTILS.makeHash(file);
            localStorage.setItem(hash, this.file.extension);
    }
    getExtansion(){
        let
            file = UTILS.arr8toStr(this.file.result),
            hash = UTILS.makeHash(file);
            return localStorage.getItem(hash);
    }
    deleteExtension(name){
        let
            arr = name.split('.');
        arr.pop();
        return arr.join('');
    }
}
doc.addEventListener('DOMContentLoaded',function(){
    let start = new Deposition_demo();
});
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	__webpack_require__(1);

	__webpack_require__(5);

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var doc = document;
	var UTILS = __webpack_require__(8).Utils;
	var KEYS = __webpack_require__(9).Keys;

	var Deposition_demo = function () {
	    function Deposition_demo() {
	        _classCallCheck(this, Deposition_demo);

	        this.dom = {
	            views: {
	                main: doc.querySelector('.b-main'),
	                success: doc.querySelector('.b-success'),
	                error: doc.querySelector('.b-error'),
	                disparity: doc.querySelector('.b-browser-disparity')
	            },
	            tabs: Array.prototype.slice.call(doc.querySelectorAll('.nav-tabs .nav-link')),
	            tabContent: Array.prototype.slice.call(doc.querySelectorAll('.tab-pane')),
	            files: {
	                decrypted: doc.querySelector('.b-file-decrypted'),
	                encrypted: doc.querySelector('.b-file-encrypted')
	            },
	            btns: {
	                encrypt: doc.querySelector('.b-btn-encrypt'),
	                decrypt: doc.querySelector('.b-btn-decrypt'),
	                to_main: Array.prototype.slice.call(doc.querySelectorAll('.b-router-main')),
	                save: doc.querySelector('.b-btn-download')
	            },
	            forms: {
	                encrypt: doc.querySelector('.b-form-encrypt'),
	                decrypt: doc.querySelector('.b-form-decrypt')
	            },
	            changeDecryptState: Array.prototype.slice.call(doc.querySelectorAll('.b-change-decrypt-state'))
	        };
	        this.file = {
	            url: '',
	            name: ''
	        };
	        this.state = {
	            fileLoaded: false
	        };
	        this.mode = 'encrypt'; //encrypt or decrypt
	        this.encryptedFileExtension = 'secret';
	        this.decryptState = 'user'; //user - расшифровка производится пользовательским ключём, 'organizations' - расшифровка происходит с исп. ключей организаций
	        typeof openpgp != 'undefined' && (window.crypto || window.msCrypto) ? this.browserDisparity = false : this.browserDisparity = true;
	        //this.browserDisparity = true;
	        !this.browserDisparity ? this.setView('main') : this.setView('disparity');
	        this.setHandlers();
	    }

	    Deposition_demo.prototype.setView = function setView(key) {
	        var section = void 0;
	        for (section in this.dom.views) {
	            this.dom.views[section].style.display = 'none';
	        }
	        this.dom.views[key].style.display = 'block';
	    };

	    Deposition_demo.prototype.setHandlers = function setHandlers() {
	        var i = void 0,
	            self = this;
	        for (i in this.dom.files) {
	            this.dom.files[i].addEventListener('change', function (e) {
	                self.uploadFile(e);
	            });
	        };
	        //encrypt button click
	        this.dom.btns.encrypt.addEventListener('click', function (e) {
	            if (self.state.fileLoaded) {
	                self.encrypt();
	                e.preventDefault();
	            }
	        });
	        //decrypt button click
	        this.dom.btns.decrypt.addEventListener('click', function (e) {
	            if (self.state.fileLoaded) {
	                self.decrypt();
	                e.preventDefault();
	            }
	        });
	        //button save encrypted/decrypted file
	        this.dom.btns.save.addEventListener('click', function (e) {
	            self.downloadFile();
	        });
	        //set decryption state (user key/organizations keys)
	        this.dom.changeDecryptState.forEach(function (el) {
	            el.addEventListener('click', function (e) {
	                self.decryptState = el.getAttribute('data-state');
	            });
	        });
	        //tabs
	        this.dom.tabs.forEach(function (tab) {
	            var targetId = void 0;
	            tab.addEventListener('click', function (e) {
	                e.preventDefault();
	                self.mode = this.getAttribute('data-section');
	                if (this.className.indexOf('active') == '-1') {
	                    targetId = this.getAttribute('href');
	                    UTILS.removeClass('active', self.dom.tabs);
	                    this.className += ' active';
	                    self.resetForms();
	                    UTILS.removeClass('active', self.dom.tabContent);
	                    doc.querySelector(targetId).className += ' active';
	                }
	            });
	        });
	    };

	    Deposition_demo.prototype.uploadFile = function uploadFile(e) {
	        var fileLink = e.target.files[0],
	            reader = new FileReader(),
	            self = this;
	        if (this.mode == 'encrypt') {
	            this.file.extension = UTILS.extension(fileLink.name);
	            this.file.name = this.deleteExtension(fileLink.name) + '.' + this.encryptedFileExtension;
	        } else {
	            this.file.name = fileLink.name;
	        }
	        reader.readAsArrayBuffer(fileLink);
	        this.file.type = fileLink.type;
	        reader.onload = function (res) {
	            var arr8 = new Uint8Array(res.target.result);
	            self.file.result = arr8;
	            if (self.mode == 'encrypt') {
	                self.saveExtension();
	            }
	            self.state.fileLoaded = true;
	        };
	    };

	    Deposition_demo.prototype.downloadFile = function downloadFile() {
	        var self = this,
	            reader = new FileReader(),
	            blob = new Blob([this.file.result]);
	        if (navigator.appVersion.toString().indexOf('.NET') > 0) {
	            //ie
	            window.navigator.msSaveBlob(blob, this.file.name);
	        } else {
	            //normal browsers
	            reader.readAsDataURL(blob);
	            reader.onload = function (res) {
	                var downloadLink = doc.createElement('a');
	                if (self.mode == 'decrypt') {
	                    var extension = self.getExtansion();
	                    self.file.name = self.deleteExtension(self.file.name) + '.' + extension;
	                }
	                downloadLink.download = self.file.name;
	                downloadLink.href = res.target.result;
	                doc.body.appendChild(downloadLink);
	                downloadLink.click();
	            };
	        }
	        self.setView('main');
	    };

	    Deposition_demo.prototype.encrypt = function encrypt() {
	        var keys = KEYS.getKeys(),
	            options = [{
	            data: this.file.result,
	            passwords: [keys.orgKey1, keys.userKey],
	            armor: false
	        }, {
	            data: '',
	            passwords: [keys.orgKey2, keys.userKey],
	            armor: false
	        }],
	            self = this;
	        //first step: uesr key + org1
	        openpgp.encrypt(options[0]).then(function (res) {
	            options[1].data = res.message.packets.write();
	            //second step: user key + org2
	            openpgp.encrypt(options[1]).then(function (res) {
	                self.file.result = res.message.packets.write();
	                self.setView('success');
	                self.resetForms();
	            }).catch(function (err) {
	                console.log('encryption error on step 1', err);
	                self.setView('error');
	            });
	        }).catch(function (err) {
	            console.log('encryption error on step 2', err);
	            self.setView('error');
	        });
	    };

	    Deposition_demo.prototype.decrypt = function decrypt() {
	        var _this = this;

	        if (this.decryptState == 'user') {
	            (function () {
	                var ff = _this.file.result,
	                    self = _this,
	                    keys = KEYS.getKeys(),
	                    options = {
	                    message: openpgp.message.read(ff),
	                    password: keys.userKey,
	                    format: 'binary'
	                };
	                openpgp.decrypt(options).then(function (res) {
	                    options.message = openpgp.message.read(res.data);
	                    openpgp.decrypt(options).then(function (res) {
	                        self.file.result = res.data;
	                        self.setView('success');
	                        self.resetForms();
	                    }).catch(function (err) {
	                        console.log('decryption error on user state, 2', err);
	                        self.setView('error');
	                    });
	                }).catch(function (err) {
	                    console.log('decryption error on user state', err);
	                    self.setView('error');
	                });
	            })();
	        } else {
	            (function () {
	                var ff = _this.file.result,
	                    self = _this,
	                    keys = KEYS.getKeys(),
	                    options = {
	                    message: openpgp.message.read(ff),
	                    password: keys.orgKey2,
	                    format: 'binary'
	                };
	                openpgp.decrypt(options).then(function (res) {
	                    options.message = openpgp.message.read(res.data);
	                    options.password = keys.orgKey1;
	                    openpgp.decrypt(options).then(function (res) {
	                        self.file.result = res.data;
	                        self.setView('success');
	                        self.resetForms();
	                    }).catch(function () {
	                        console.log('decryption error on org state, 2', err);
	                        self.setView('error');
	                    });
	                }).catch(function (err) {
	                    console.log('decryption error on org state, 1', err);
	                    self.setView('error');
	                });
	            })();
	        }
	    };

	    Deposition_demo.prototype.resetForms = function resetForms() {
	        this.dom.forms.encrypt.reset();
	        this.dom.forms.decrypt.reset();
	    };

	    Deposition_demo.prototype.saveExtension = function saveExtension() {
	        var file = UTILS.arr8toStr(this.file.result),
	            hash = UTILS.makeHash(file);
	        localStorage.setItem(hash, this.file.extension);
	    };

	    Deposition_demo.prototype.getExtansion = function getExtansion() {
	        var file = UTILS.arr8toStr(this.file.result),
	            hash = UTILS.makeHash(file);
	        return localStorage.getItem(hash);
	    };

	    Deposition_demo.prototype.deleteExtension = function deleteExtension(name) {
	        var arr = name.split('.');
	        arr.pop();
	        return arr.join('');
	    };

	    return Deposition_demo;
	}();

	doc.addEventListener('DOMContentLoaded', function () {
	    var start = new Deposition_demo();
	});

/***/ },
/* 1 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 6 */,
/* 7 */,
/* 8 */
/***/ function(module, exports) {

	'use strict';

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var Utils = function () {
	    function Utils() {
	        _classCallCheck(this, Utils);
	    }

	    Utils.getParents = function getParents(el, parentSelector) {
	        // If no parentSelector defined will bubble up all the way to *document*
	        if (parentSelector === undefined) {
	            parentSelector = document;
	        }

	        var parents = [];
	        var p = el.parentNode;

	        while (p !== parentSelector) {
	            var o = p;
	            parents.push(o);
	            p = o.parentNode;
	        }
	        parents.push(parentSelector); // Push that parentSelector you wanted to stop at

	        return parents;
	    };

	    Utils.removeClass = function removeClass(class_str, dom) {
	        var temp = void 0;
	        if (dom.length == undefined) {
	            temp = dom.className.replace(' ' + class_str, '');
	            dom.className = temp;
	        } else {
	            dom.forEach(function (el) {
	                temp = el.className.replace(' ' + class_str, '');
	                el.className = temp;
	            });
	        }
	    };

	    Utils.showDom = function showDom(dom) {
	        if (dom.length == undefined) {
	            dom.style.display = 'inline-block';
	        } else {
	            dom.forEach(function (el) {
	                el.style.display = 'inline-block';
	            });
	        }
	    };

	    Utils.hideDom = function hideDom(dom) {
	        if (dom.length == undefined) {
	            dom.style.display = 'none';
	        } else {
	            dom.forEach(function (el) {
	                el.style.display = 'none';
	            });
	        }
	    };

	    Utils.makeHash = function makeHash(str) {
	        /*весь метод - сторонний кусок кода*/
	        var RotateLeft = function RotateLeft(lValue, iShiftBits) {
	            return lValue << iShiftBits | lValue >>> 32 - iShiftBits;
	        };

	        var AddUnsigned = function AddUnsigned(lX, lY) {
	            var lX4, lY4, lX8, lY8, lResult;
	            lX8 = lX & 0x80000000;
	            lY8 = lY & 0x80000000;
	            lX4 = lX & 0x40000000;
	            lY4 = lY & 0x40000000;
	            lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);
	            if (lX4 & lY4) {
	                return lResult ^ 0x80000000 ^ lX8 ^ lY8;
	            }
	            if (lX4 | lY4) {
	                if (lResult & 0x40000000) {
	                    return lResult ^ 0xC0000000 ^ lX8 ^ lY8;
	                } else {
	                    return lResult ^ 0x40000000 ^ lX8 ^ lY8;
	                }
	            } else {
	                return lResult ^ lX8 ^ lY8;
	            }
	        };

	        var F = function F(x, y, z) {
	            return x & y | ~x & z;
	        };
	        var G = function G(x, y, z) {
	            return x & z | y & ~z;
	        };
	        var H = function H(x, y, z) {
	            return x ^ y ^ z;
	        };
	        var I = function I(x, y, z) {
	            return y ^ (x | ~z);
	        };

	        var FF = function FF(a, b, c, d, x, s, ac) {
	            a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
	            return AddUnsigned(RotateLeft(a, s), b);
	        };

	        var GG = function GG(a, b, c, d, x, s, ac) {
	            a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
	            return AddUnsigned(RotateLeft(a, s), b);
	        };

	        var HH = function HH(a, b, c, d, x, s, ac) {
	            a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
	            return AddUnsigned(RotateLeft(a, s), b);
	        };

	        var II = function II(a, b, c, d, x, s, ac) {
	            a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
	            return AddUnsigned(RotateLeft(a, s), b);
	        };

	        var ConvertToWordArray = function ConvertToWordArray(str) {
	            var lWordCount;
	            var lMessageLength = str.length;
	            var lNumberOfWords_temp1 = lMessageLength + 8;
	            var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - lNumberOfWords_temp1 % 64) / 64;
	            var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
	            var lWordArray = Array(lNumberOfWords - 1);
	            var lBytePosition = 0;
	            var lByteCount = 0;
	            while (lByteCount < lMessageLength) {
	                lWordCount = (lByteCount - lByteCount % 4) / 4;
	                lBytePosition = lByteCount % 4 * 8;
	                lWordArray[lWordCount] = lWordArray[lWordCount] | str.charCodeAt(lByteCount) << lBytePosition;
	                lByteCount++;
	            }
	            lWordCount = (lByteCount - lByteCount % 4) / 4;
	            lBytePosition = lByteCount % 4 * 8;
	            lWordArray[lWordCount] = lWordArray[lWordCount] | 0x80 << lBytePosition;
	            lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
	            lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
	            return lWordArray;
	        };

	        var WordToHex = function WordToHex(lValue) {
	            var WordToHexValue = "",
	                WordToHexValue_temp = "",
	                lByte,
	                lCount;
	            for (lCount = 0; lCount <= 3; lCount++) {
	                lByte = lValue >>> lCount * 8 & 255;
	                WordToHexValue_temp = "0" + lByte.toString(16);
	                WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length - 2, 2);
	            }
	            return WordToHexValue;
	        };

	        var x = Array();
	        var k, AA, BB, CC, DD, a, b, c, d;
	        var S11 = 7,
	            S12 = 12,
	            S13 = 17,
	            S14 = 22;
	        var S21 = 5,
	            S22 = 9,
	            S23 = 14,
	            S24 = 20;
	        var S31 = 4,
	            S32 = 11,
	            S33 = 16,
	            S34 = 23;
	        var S41 = 6,
	            S42 = 10,
	            S43 = 15,
	            S44 = 21;

	        str = this.utf8_encode(str);
	        x = ConvertToWordArray(str);
	        a = 0x67452301;b = 0xEFCDAB89;c = 0x98BADCFE;d = 0x10325476;

	        for (k = 0; k < x.length; k += 16) {
	            AA = a;BB = b;CC = c;DD = d;
	            a = FF(a, b, c, d, x[k + 0], S11, 0xD76AA478);
	            d = FF(d, a, b, c, x[k + 1], S12, 0xE8C7B756);
	            c = FF(c, d, a, b, x[k + 2], S13, 0x242070DB);
	            b = FF(b, c, d, a, x[k + 3], S14, 0xC1BDCEEE);
	            a = FF(a, b, c, d, x[k + 4], S11, 0xF57C0FAF);
	            d = FF(d, a, b, c, x[k + 5], S12, 0x4787C62A);
	            c = FF(c, d, a, b, x[k + 6], S13, 0xA8304613);
	            b = FF(b, c, d, a, x[k + 7], S14, 0xFD469501);
	            a = FF(a, b, c, d, x[k + 8], S11, 0x698098D8);
	            d = FF(d, a, b, c, x[k + 9], S12, 0x8B44F7AF);
	            c = FF(c, d, a, b, x[k + 10], S13, 0xFFFF5BB1);
	            b = FF(b, c, d, a, x[k + 11], S14, 0x895CD7BE);
	            a = FF(a, b, c, d, x[k + 12], S11, 0x6B901122);
	            d = FF(d, a, b, c, x[k + 13], S12, 0xFD987193);
	            c = FF(c, d, a, b, x[k + 14], S13, 0xA679438E);
	            b = FF(b, c, d, a, x[k + 15], S14, 0x49B40821);
	            a = GG(a, b, c, d, x[k + 1], S21, 0xF61E2562);
	            d = GG(d, a, b, c, x[k + 6], S22, 0xC040B340);
	            c = GG(c, d, a, b, x[k + 11], S23, 0x265E5A51);
	            b = GG(b, c, d, a, x[k + 0], S24, 0xE9B6C7AA);
	            a = GG(a, b, c, d, x[k + 5], S21, 0xD62F105D);
	            d = GG(d, a, b, c, x[k + 10], S22, 0x2441453);
	            c = GG(c, d, a, b, x[k + 15], S23, 0xD8A1E681);
	            b = GG(b, c, d, a, x[k + 4], S24, 0xE7D3FBC8);
	            a = GG(a, b, c, d, x[k + 9], S21, 0x21E1CDE6);
	            d = GG(d, a, b, c, x[k + 14], S22, 0xC33707D6);
	            c = GG(c, d, a, b, x[k + 3], S23, 0xF4D50D87);
	            b = GG(b, c, d, a, x[k + 8], S24, 0x455A14ED);
	            a = GG(a, b, c, d, x[k + 13], S21, 0xA9E3E905);
	            d = GG(d, a, b, c, x[k + 2], S22, 0xFCEFA3F8);
	            c = GG(c, d, a, b, x[k + 7], S23, 0x676F02D9);
	            b = GG(b, c, d, a, x[k + 12], S24, 0x8D2A4C8A);
	            a = HH(a, b, c, d, x[k + 5], S31, 0xFFFA3942);
	            d = HH(d, a, b, c, x[k + 8], S32, 0x8771F681);
	            c = HH(c, d, a, b, x[k + 11], S33, 0x6D9D6122);
	            b = HH(b, c, d, a, x[k + 14], S34, 0xFDE5380C);
	            a = HH(a, b, c, d, x[k + 1], S31, 0xA4BEEA44);
	            d = HH(d, a, b, c, x[k + 4], S32, 0x4BDECFA9);
	            c = HH(c, d, a, b, x[k + 7], S33, 0xF6BB4B60);
	            b = HH(b, c, d, a, x[k + 10], S34, 0xBEBFBC70);
	            a = HH(a, b, c, d, x[k + 13], S31, 0x289B7EC6);
	            d = HH(d, a, b, c, x[k + 0], S32, 0xEAA127FA);
	            c = HH(c, d, a, b, x[k + 3], S33, 0xD4EF3085);
	            b = HH(b, c, d, a, x[k + 6], S34, 0x4881D05);
	            a = HH(a, b, c, d, x[k + 9], S31, 0xD9D4D039);
	            d = HH(d, a, b, c, x[k + 12], S32, 0xE6DB99E5);
	            c = HH(c, d, a, b, x[k + 15], S33, 0x1FA27CF8);
	            b = HH(b, c, d, a, x[k + 2], S34, 0xC4AC5665);
	            a = II(a, b, c, d, x[k + 0], S41, 0xF4292244);
	            d = II(d, a, b, c, x[k + 7], S42, 0x432AFF97);
	            c = II(c, d, a, b, x[k + 14], S43, 0xAB9423A7);
	            b = II(b, c, d, a, x[k + 5], S44, 0xFC93A039);
	            a = II(a, b, c, d, x[k + 12], S41, 0x655B59C3);
	            d = II(d, a, b, c, x[k + 3], S42, 0x8F0CCC92);
	            c = II(c, d, a, b, x[k + 10], S43, 0xFFEFF47D);
	            b = II(b, c, d, a, x[k + 1], S44, 0x85845DD1);
	            a = II(a, b, c, d, x[k + 8], S41, 0x6FA87E4F);
	            d = II(d, a, b, c, x[k + 15], S42, 0xFE2CE6E0);
	            c = II(c, d, a, b, x[k + 6], S43, 0xA3014314);
	            b = II(b, c, d, a, x[k + 13], S44, 0x4E0811A1);
	            a = II(a, b, c, d, x[k + 4], S41, 0xF7537E82);
	            d = II(d, a, b, c, x[k + 11], S42, 0xBD3AF235);
	            c = II(c, d, a, b, x[k + 2], S43, 0x2AD7D2BB);
	            b = II(b, c, d, a, x[k + 9], S44, 0xEB86D391);
	            a = AddUnsigned(a, AA);
	            b = AddUnsigned(b, BB);
	            c = AddUnsigned(c, CC);
	            d = AddUnsigned(d, DD);
	        }

	        var temp = WordToHex(a) + WordToHex(b) + WordToHex(c) + WordToHex(d);

	        return temp.toLowerCase();
	    };

	    Utils.utf8_encode = function utf8_encode(str_data) {
	        /*весь метод - сторонний кусок кода*/
	        str_data = str_data.replace(/\r\n/g, "\n");
	        var utftext = "";
	        for (var n = 0; n < str_data.length; n++) {
	            var c = str_data.charCodeAt(n);
	            if (c < 128) {
	                utftext += String.fromCharCode(c);
	            } else if (c > 127 && c < 2048) {
	                utftext += String.fromCharCode(c >> 6 | 192);
	                utftext += String.fromCharCode(c & 63 | 128);
	            } else {
	                utftext += String.fromCharCode(c >> 12 | 224);
	                utftext += String.fromCharCode(c >> 6 & 63 | 128);
	                utftext += String.fromCharCode(c & 63 | 128);
	            }
	        }
	        return utftext;
	    };

	    Utils.arr8toStr = function arr8toStr(myUint8Arr) {
	        return String.fromCharCode.apply(null, myUint8Arr);
	    };

	    Utils.extension = function extension(str) {
	        var nameArr = str.split('.'),
	            extension = nameArr[nameArr.length - 1];
	        return extension;
	    };

	    return Utils;
	}();

	exports.Utils = Utils;

/***/ },
/* 9 */
/***/ function(module, exports) {

	'use strict';

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var Keys = function () {
	    function Keys() {
	        _classCallCheck(this, Keys);
	    }

	    Keys.getKeys = function getKeys() {
	        var keys = void 0;
	        /* TEMP */
	        // keys = {
	        //     userKey: 'key1',
	        //     orgKey1: 'key2',
	        //     orgKey2: 'key3'
	        // };

	        keys = {
	            userKey: '-----BEGIN%20PGP%20PUBLIC%20KEY%20BLOCK-----%0D%0AVersion%3A%20OpenPGP.js%20v2.3.3%0D%0AComment%3A%20http%3A%2F%2Fopenpgpjs.org%0D%0A%0D%0AxsBNBFgbHzABCADjNCqLybp4Fs1wXr%2BWySiDJd1PQtnGeKNqD8krIzoQb%2F9m%0AtRcZyz2tVs%2F4Dw3V0LWntK1GlldRjpkm3w9iPsMqMzLVsu6TEDAz4l1f8IaS%0AadVZdjqnQI11SMegXBav%2BPuRVvseGBqSOVayiAODNOyPF8fy%2FHX4odOIsYSI%0A3suFkmSKKjLG4AVsLFH1SSzAF14NriZArqrpbN%2BYd52tSFyc0Cd8junVbanK%0ArVCrnbWuJZg4dLOKL2fd5mNUEIR5Gz96fCgq8%2FcpbN9j%2BLu099m9ZRC62EBr%0AkOwYiUFtnMRoLn31h8f9oXMfWqJ4ruDOGt4seZ9vJVkJbGoCSFlDMESHABEB%0AAAHNAyA8PsLAdQQQAQgAKQUCWBsfMAYLCQcIAwIJEBK7rHGGkxxdBBUIAgoD%0AFgIBAhkBAhsDAh4BAAAK0Af%2BPXhwjaJ6%2F9suxG96k9UMIbxmZEhaV%2BBcNWtD%0AEnZdEJpPi%2B9CLSxlpEzvA7ubhVXiysAVIRZstUyxthcoWPFi%2FaLe7h7YwFbx%0A1wgxlJ6EJhGxEV62Cfs4nKE0PrYc7u%2B5OG%2BFrULPsEwJdR0Yia34ofJdDIj4%0AwP%2F7o%2FSkWIpDAPYoKs2Mx0TmtgDxXgZFM194hGPcOvgl7rS0YZ33tmccsEcx%0AEOREEKVUa%2BC9QSm4d8mZkfR0a4X2Y%2Ff9cqTq%2FB1Zg6Qtdub7qHe%2FLseB7wgf%0ArbcOLF2W%2Fk%2BKu7aj1PojRTqepdEZz5jzz7M%2Fv3%2FgifF5p6Hkm7jSCBP5YJAT%0A%2BO56a87ATQRYGx8wAQgA3DuabwlQPDinBY%2FvRpPzdFHjLdKCevhDVtwQSYWR%0A0M%2Bz5%2Fj45rd%2BDZXYd5u3Uw%2F%2BBPR6gtlLG7xtA7%2FDlMzPpa76C0hlhpX3lKuU%0AVenxIIeCwHqjJZs%2FcdG5ctAQRrc76bA%2FJ4McP8fN2138CdJhvWNObqExB69N%0AeZiLdWva6fVf%2F8uxRBpn1obpirvVyhMxioILHhJAHwiFfs94MwuQbxzmrddm%0A4hxN8Tmzvhg9inTe0l9QTnLJjXPq1iQOvpgB27ljnXPZG2VbA%2BUBaz1tOSAE%0ArPHVTKO%2FesowmwiDlpaNa0DoTKcvu88K%2BdU2PY%2FKzQmInVPJ%2F87ExSdfPwXe%0A%2FQARAQABwsBfBBgBCAATBQJYGx8wCRASu6xxhpMcXQIbDAAAL3gH%2F3A1XFGi%0Ak8lZhVnYRD9FwmQU5%2BKOnRABSumsM5Dicj4pnHhVmeokntLCQyE9dyNM6zjV%0AOvL6brjB4tBwkq71iv%2B41HrZOUy%2F8uHHDOb9MqZS6poFzeOeoAS0QT26RGe2%0A%2FV2Trx87Hn%2BEQX9AKK5UaxosiDneuU78v9xciBlsZN9GvHTV1t8BWlO%2BQKfe%0AKyWkpGMe5Dh9HjOol7WLJ1V7j3JODqIc7abip%2BGlvyKdxCQip4O9HqlgdxM8%0ABSu2FwPeHUTwiHr80zm0k8j2FYN7icBEx9xGUXzfnkapk1SWar2t%2F%2Bm58FXB%0AYwg3Zin3PFSv20AXQtbQ3oJEzpWUydMn53o%3D%0D%0A%3DyTt7%0D%0A-----END%20PGP%20PUBLIC%20KEY%20BLOCK-----%0D%0A%0D%0A',
	            orgKey1: '-----BEGIN%20PGP%20PUBLIC%20KEY%20BLOCK-----%0D%0AVersion%3A%20OpenPGP.js%20v2.3.3%0D%0AComment%3A%20http%3A%2F%2Fopenpgpjs.org%0D%0A%0D%0AxsBNBFgbH4YBCADPvpxmqGsMKIUJIWGvx4uROwYPJ%2B4w9R%2BS4%2FRgJ9JwdqfF%0A18adDCx%2BekEQXPSQoaWSTjBaq8CCDM3uBBfPiDoO5RW2stuQXw%2BcReWmkyfT%0AL2i6TR5717e43CZbj2IMH0kznFFhxaQOREUUNpmn5oeKVpeHIam8l520%2Bef1%0Af%2B9Kc%2Bznh0QaR2gVw9u9rCEvSz1aCLxwL5OGhYdZzbhEdER%2BhmiG1YD8uBKY%0ALsqAlV4%2Fe7FYuk%2BZhvwiJpCydZaVCiR8lYu%2F91rTWT9LcXfLsatxvbBcxx9F%0ASkLa6ROQUxRkSUSs5JejdJBNDVdkOz1LR6xUQe89MnjQX71yxhD5CCEHABEB%0AAAHNAyA8PsLAdQQQAQgAKQUCWBsfhgYLCQcIAwIJEMlsm3h9Eb1JBBUIAgoD%0AFgIBAhkBAhsDAh4BAABsjggAggxjCqDvTPpdEPJ20CB1EytI74G37Gfpiw8O%0AdWGuwXmBWneTR6QzVuocxRtORupiwhIXGU9oLWnWARhGNwFBwnR4Fd0LSdq9%0AU77L4HdOQTb%2FxbwOg48V%2BMCDvJGMjpa3p1teiSm4Wa7RPkS8IDamW9u1%2Blhh%0ALSS4pDOre0aQMonUwakTvPHV2%2BH%2B80FS6GAkqGEsNOsYqcGehGQWgYmj5dgz%0AVebT%2Bso1VM%2BrfTn5XJKfDs94tlxmQlRCU6iWSSozcliD9uXk1eZ6F6fBceLx%0A2ohgCEroCpYGafKu6HFwA1pfO3jEXOFqtr13rnPO%2B6dz0Tae1Ibrc7IeIONb%0AY9P1dc7ATQRYGx%2BGAQgAy5kgNkbVTa6BfoR%2FIzdDQcBBl3Lau3qctsVLqzTZ%0Api0PqnN9cS%2BxfnrWZZO09qr%2Bp0JAH2TiqmR%2Fd5FCHRiR7bhZf1JF3qowThRu%0Am0zgF%2B24L3aFsUGVvMcvV7Xn9IRmnP2nh%2FSmNkwZ0VTxIRGYlhrPtve43v%2Bt%0APyujGVEAdbgFekeK6uMGMvmQXds4zXS3%2BpAnQ3JqbuM7ZaHX1UiBGbXVcLrP%0Ao7X3wok4%2Bnm%2FpH6IHl6mjcYEBDF7yxKoo0kaKVL6r%2FhuiEqFDO3IVinY%2F3kD%0AaxtLI%2Bh7uEAXqpLkkF0J3xC%2B4a%2BXV8eOzgKhm5uUZBzZwYDKzMGIT52wYSIg%0AOwARAQABwsBfBBgBCAATBQJYGx%2BHCRDJbJt4fRG9SQIbDAAAIt4H%2Fjn38Z5q%0AbVuzQQcGEvd72G3VwWoIuDBQx9twpRtar6TW33L5G2Kq8u5ry3v3wHxUY8p0%0A2HsYtRmCtbPGtVfgLI53d%2BiLh%2BhR540aQv3BM%2FPRL2K5ERRKdsJjMF61ltsn%0Ar3m98RqrztVkEJdQ6TAVYCsdxHrJC7fwTS%2FJQRBhx5sHEeiu1EUGY0iRO6ty%0AwLAWu0eVnddIllqrH7WJRce485Y70Njh8Pvk%2Fr45GYCOsUYeBuwEt22psS%2Bm%0Azl0gcHqySNfgMJQjMr8voouxDGKMq%2FopHxLSk6NtOL2bAxrl1KMypttVfiBf%0AgJNzswo24G9e9GLot%2Fjqd8yD2EErqh87vAE%3D%0D%0A%3Dy9at%0D%0A-----END%20PGP%20PUBLIC%20KEY%20BLOCK-----%0D%0A%0D%0A',
	            orgKey2: '-----BEGIN%20PGP%20PUBLIC%20KEY%20BLOCK-----%0D%0AVersion%3A%20OpenPGP.js%20v2.3.3%0D%0AComment%3A%20http%3A%2F%2Fopenpgpjs.org%0D%0A%0D%0AxsBNBFgbIA8BCAC%2Bj0TdoQAU4dBODVS6gSDjoemob2yqcuGET%2F7VqOujJBO2%0A3Kj2jI7piAkdzryYOuD6C2R7ZPctAUG2bUoF7p9jBtr%2FIc3C7O6s4NEcIC22%0Agmzwi8zLn96ozgTCWqmsu7iYDaHR0JE5fi19h6Xb3eMEELPLZfgZpaEaqSRd%0A4QsPLOj2GRmrrJ98ZTb7jY3u0%2B%2B68Dosdp4FUCiRSM67Kmc1%2B2mVrOqhKZhw%0AAKvQOe8MfUBVk7UiyMG3yOMjcbmFobfuiRCIV24m0iCto%2BG%2F2j2m%2BMbnLJ4W%0AMttCs6ycGXgfkJdp0bUXpcBAXQToHLqgRw6QrGqCT5aw0vstRTPwer95ABEB%0AAAHNAyA8PsLAdQQQAQgAKQUCWBsgDwYLCQcIAwIJEHTcgrOdBmolBBUIAgoD%0AFgIBAhkBAhsDAh4BAADZiQf%2FW9VBmPqPYwOGQELqYkwTxVaYYyiuNwiHe2T%2F%0AF103bGnwsex3olyVzn2FLVFbIL8wfKBKbG2e%2BHSH4jbZ7v%2BaKmBwv9p1olIb%0AissQ%2BHSzj9wDNabZqjHsWda4zC%2FOTFUs8HIU2IZtHgq%2BIxh90Bek45ht4%2FGD%0AVBW%2Fij%2BdodS7TjZvT%2BWC08MyE4LtMjKuBG4o4ThWEGpS0D%2B9UMbMh3%2B%2B427I%0ArggZK%2BbtZ%2B6ghdwUufgDKcc%2F7R2tBoTMmytkQnFJ0BjlkHw%2FCLMHOsq3KG0D%0AYSxKmgig0p9sMbsZ6etnSOteUMOn7St1PvHogMq61PLxPJrFj2cR4hHDgvvz%0A3jJtBc7ATQRYGyAPAQgAxV8tAF3kkh7AZwlhan4BQw4vv5VFauzJjh5%2FH1RP%0AwpsW261E6vPXEXqP3wDXsJf5oPgOcr21VIGDTrmrqBIDSUj%2FY4LEwPKSMAEq%0AXMdPDSHIbiAhcZgvVTZUci01yYXaIyh550UdH%2FdfvvTpaksUpWBs1KUamxod%0AxZXy3qzwy9pR59OYl5VxhBdeRWdGO9sy%2BjgqZVGOgAKmlFKoHZNhKIHYd2wF%0AGoiz%2B6JjKbMB9S6URYfqWfsYSubsgRGzO9nzAf8YrUquBoGk2tUIAWCAF%2FSh%0ABu0nqBZtol9Jvw%2F%2BYSy5Rzj3Nt5pPqaKBtrJohEGw4FDJgqnOatzySk7Dofh%0AUwARAQABwsBfBBgBCAATBQJYGyAQCRB03IKznQZqJQIbDAAA6jEH%2FRznwHYa%0A2k0Rhn%2FNxjbsq8vLouHyx40Bjk%2F1cjMgOei5mecbDm0BJ04yaTSkqjpDyZRp%0Al90SPPnlKT8wL5IqrECCbo16Bp%2FOgiuM5jh5tlU3kPq8EocuDCzhxyWwhBzz%0AqWfK%2FM5r4oRKpOa5GqFGDRPJPtiWPEb%2FHI2Te%2FY2Cp3eO6E7HOLroDqCdLap%0AukXUdZCg6XQRbWaQEJW1iFb%2Fr6cc8lQUstWBDGRIE%2BaklZVV9yJoVAQ1GUbZ%0A8VGxqMZyXePQ7NJyJOTRfbuKhv5UuNc%2FUJ8JEgm9L%2B6BenTzE0Mn%2FQOKocvH%0A%2FwSIe1nhV4mfvIdvj9Dz06PQAxrXmvso4%2B4%3D%0D%0A%3DE9fI%0D%0A-----END%20PGP%20PUBLIC%20KEY%20BLOCK-----%0D%0A%0D%0A'
	        };

	        // keys = {
	        //     userKey: {
	        //         private: "-----BEGIN%20PGP%20PRIVATE%20KEY%20BLOCK-----%0D%0AVersion%3A%20OpenPGP.js%20v2.3.3%0D%0AComment%3A%20http%3A%2F%2Fopenpgpjs.org%0D%0A%0D%0AxcMGBFgbHzABCADjNCqLybp4Fs1wXr%2BWySiDJd1PQtnGeKNqD8krIzoQb%2F9m%0AtRcZyz2tVs%2F4Dw3V0LWntK1GlldRjpkm3w9iPsMqMzLVsu6TEDAz4l1f8IaS%0AadVZdjqnQI11SMegXBav%2BPuRVvseGBqSOVayiAODNOyPF8fy%2FHX4odOIsYSI%0A3suFkmSKKjLG4AVsLFH1SSzAF14NriZArqrpbN%2BYd52tSFyc0Cd8junVbanK%0ArVCrnbWuJZg4dLOKL2fd5mNUEIR5Gz96fCgq8%2FcpbN9j%2BLu099m9ZRC62EBr%0AkOwYiUFtnMRoLn31h8f9oXMfWqJ4ruDOGt4seZ9vJVkJbGoCSFlDMESHABEB%0AAAH%2BCQMIQPrvEUe1jeNgQvwzNKcjhNOL6wexkmh%2BCrJF3Po8R6YMrQw8587A%0AFdiAE%2FxGIUNw7KSp4HkwfTtvSVPpZIiAHUkJgsiL6zZF%2FZJVJrhV%2BHvbyDva%0AzjHt3K%2BbGM6z57BI8CS8YI8DKqg0HHKCh0CSmwINo%2FVrh1jdEGlVCogpmS32%0AekEy0%2BNUHSKclJBcZC03bjiUI6xxs4ccBJJ6v2zFpS7PNysOl%2FmctT6HIMOJ%0A4xQThSAYnxwVuIt7JbXFny15bOhDXCESXzulRF96djedEDLMfl9k1LyzTZum%0ArUdWeBazQHykZpmtR1AR%2BfSKMITdlTSPxZ%2Fs%2FHybyAVGsO%2FOjb8oGfaS2uUS%0AjNi3OjMd77Gy%2FCqOV3d7b8Wz342wrBSIVHOc1yRXeky5IcdJcsh5N7FEftsj%0AI2gZgNmGcMotgn6ug5wGYyPsEyQPRQzCzhJej0fELuTuhjKBLkrBQtitmTE1%0A0jR7rFwyCgMtLjZOrJM9AssGfHlwbdlcHFu9hFiNwPMDhaANXodXNRXjSirh%0ASmUEadbHLZ%2FgugbnLyVUwZFF81NzU9a%2BomrPLiGSuvGCZJiTDr7cyfOD30zZ%0AP8hn1xeqGvW9HKEpYO7sB%2BRFJRsW0%2Fes%2BqJcN6aAgKERpJUiGvEYVw%2F6Brwk%0A198Kezw0ez7xsfrsmraOZd0JwMsDz%2BV93fWB%2Fxr92jCDsUE2pyFFbkKeOaUC%0AyMHRqXQzzBmvXkeDnQrGZLK14pkx4SWgmSQB5IAqGwoYnfBvzsLSfA3GEflc%0AymABL9QYhyQNRWtOlWm3UjvxBC4A8Hqb1vac62s%2FKMYKNsHRFfjGEjKGdeBF%0AMr%2FhmpsAZJF%2BCmEoS4chGen%2BTMskuKrkwuezvPs1nABPrXzj1guaPz8OVeXI%0AwEG%2FLc5pmzHTBclmxdIjYtM3pcItMAIQzQMgPD7CwHUEEAEIACkFAlgbHzAG%0ACwkHCAMCCRASu6xxhpMcXQQVCAIKAxYCAQIZAQIbAwIeAQAACtAH%2Fj14cI2i%0Aev%2FbLsRvepPVDCG8ZmRIWlfgXDVrQxJ2XRCaT4vvQi0sZaRM7wO7m4VV4srA%0AFSEWbLVMsbYXKFjxYv2i3u4e2MBW8dcIMZSehCYRsRFetgn7OJyhND62HO7v%0AuThvha1Cz7BMCXUdGImt%2BKHyXQyI%2BMD%2F%2B6P0pFiKQwD2KCrNjMdE5rYA8V4G%0ARTNfeIRj3Dr4Je60tGGd97ZnHLBHMRDkRBClVGvgvUEpuHfJmZH0dGuF9mP3%0A%2FXKk6vwdWYOkLXbm%2B6h3vy7Hge8IH623Dixdlv5Piru2o9T6I0U6nqXRGc%2BY%0A88%2BzP79%2F4Inxeaeh5Ju40ggT%2BWCQE%2FjuemvHwwYEWBsfMAEIANw7mm8JUDw4%0ApwWP70aT83RR4y3Sgnr4Q1bcEEmFkdDPs%2Bf4%2BOa3fg2V2Hebt1MP%2FgT0eoLZ%0ASxu8bQO%2Fw5TMz6Wu%2BgtIZYaV95SrlFXp8SCHgsB6oyWbP3HRuXLQEEa3O%2Bmw%0APyeDHD%2FHzdtd%2FAnSYb1jTm6hMQevTXmYi3Vr2un1X%2F%2FLsUQaZ9aG6Yq71coT%0AMYqCCx4SQB8IhX7PeDMLkG8c5q3XZuIcTfE5s74YPYp03tJfUE5yyY1z6tYk%0ADr6YAdu5Y51z2RtlWwPlAWs9bTkgBKzx1Uyjv3rKMJsIg5aWjWtA6EynL7vP%0ACvnVNj2Pys0JiJ1Tyf%2FOxMUnXz8F3v0AEQEAAf4JAwj3EmQKb1ew0mAF0Ww7%0Awc5x5Kz%2FM%2FCmjREu%2FOcPf2fDu4BKmxt1F7i5Nz1PVO5vA9qVxUlz7j5t3qVg%0A7sZXS7m1RFhArUiXERvQp4aCh3GfmVMk1v%2FHD7vlTBgOoTGC4yQUGljRkqqi%0AE6sF%2BlKGqePIjaI1mvSvyulsmApMpFHRwEuC4l%2FO2lYKRIxJ4HmfLxdQZzdc%0AtmBxCNSGOFLeuPBAZCg1NQi5ZazHuwFO8tMh9vugYSrXoTy4W1qAkD7q%2FLXN%0ASRAXWXkPXHLBFnJA7iQiu9TcIc2XpANr1EJefTCBKJgilirB6TPDMYpurCdD%0AdpQPpiInfOyHefPv3IuwdhsUL%2BG3sPaohc%2Fmx7AySssnDR7yKzNVrE4DqLTR%0AHowdYg6XWB5C8RDRzIpdyD1YncQASRhK%2Bqh7W4apidXBiFoDKSV36ebK%2BOSI%0A1Nlj5xAetj97I783KjAZ89hzviyPoCqVd2FH39%2BQI8tmlC31RjlRlszcOURQ%0AQKuUmu%2FcfY8KUMEc9j51oUQf69XyQ%2FQ46QIynp2JdkKn4Gw7iZjchRO6%2Bqzo%0AxNhXaFfQ%2B8gTo8aQj%2BWHBhZ6qrA6reBl6HF4XIAPRVpBd%2BL%2F%2BEIjISP1Wdgd%0A9JiMzKtS3W0p9vTSVEwi7duGPYXUfBn%2B11gmDFOuMcv9etOsQjK438La5bVO%0AU%2FD1i%2BR74uNpYaNYDYI4xgS4Jn1t5EmVFWlJTQuXy6bduDkZRPqH4IQ8NpkN%0Ago%2BGpIaS7NIYCDYSWIYWIt2SW%2BAjDKXlftPhFBd0en2uodO02hD9xydqZxu9%0AKP8RdZOJslyCmd7LXRtRaV3AxE3g0HRlHFhAaf%2BPsjBVs6uS2xw4kO6iN6N1%0APOzm9ElrOqYwg4UaVuujfKRC16iJDaAYVhGOg1qGOW3whc7ciCyByUgta%2Byf%0A0W1KbejCwF8EGAEIABMFAlgbHzAJEBK7rHGGkxxdAhsMAAAveAf%2FcDVcUaKT%0AyVmFWdhEP0XCZBTn4o6dEAFK6awzkOJyPimceFWZ6iSe0sJDIT13I0zrONU6%0A8vpuuMHi0HCSrvWK%2F7jUetk5TL%2Fy4ccM5v0yplLqmgXN456gBLRBPbpEZ7b9%0AXZOvHzsef4RBf0AorlRrGiyIOd65Tvy%2F3FyIGWxk30a8dNXW3wFaU75Ap94r%0AJaSkYx7kOH0eM6iXtYsnVXuPck4OohztpuKn4aW%2FIp3EJCKng70eqWB3EzwF%0AK7YXA94dRPCIevzTObSTyPYVg3uJwETH3EZRfN%2BeRqmTVJZqva3%2F6bnwVcFj%0ACDdmKfc8VK%2FbQBdC1tDegkTOlZTJ0yfneg%3D%3D%0D%0A%3DAjZO%0D%0A-----END%20PGP%20PRIVATE%20KEY%20BLOCK-----%0D%0A",
	        //         public: "-----BEGIN%20PGP%20PUBLIC%20KEY%20BLOCK-----%0D%0AVersion%3A%20OpenPGP.js%20v2.3.3%0D%0AComment%3A%20http%3A%2F%2Fopenpgpjs.org%0D%0A%0D%0AxsBNBFgbHzABCADjNCqLybp4Fs1wXr%2BWySiDJd1PQtnGeKNqD8krIzoQb%2F9m%0AtRcZyz2tVs%2F4Dw3V0LWntK1GlldRjpkm3w9iPsMqMzLVsu6TEDAz4l1f8IaS%0AadVZdjqnQI11SMegXBav%2BPuRVvseGBqSOVayiAODNOyPF8fy%2FHX4odOIsYSI%0A3suFkmSKKjLG4AVsLFH1SSzAF14NriZArqrpbN%2BYd52tSFyc0Cd8junVbanK%0ArVCrnbWuJZg4dLOKL2fd5mNUEIR5Gz96fCgq8%2FcpbN9j%2BLu099m9ZRC62EBr%0AkOwYiUFtnMRoLn31h8f9oXMfWqJ4ruDOGt4seZ9vJVkJbGoCSFlDMESHABEB%0AAAHNAyA8PsLAdQQQAQgAKQUCWBsfMAYLCQcIAwIJEBK7rHGGkxxdBBUIAgoD%0AFgIBAhkBAhsDAh4BAAAK0Af%2BPXhwjaJ6%2F9suxG96k9UMIbxmZEhaV%2BBcNWtD%0AEnZdEJpPi%2B9CLSxlpEzvA7ubhVXiysAVIRZstUyxthcoWPFi%2FaLe7h7YwFbx%0A1wgxlJ6EJhGxEV62Cfs4nKE0PrYc7u%2B5OG%2BFrULPsEwJdR0Yia34ofJdDIj4%0AwP%2F7o%2FSkWIpDAPYoKs2Mx0TmtgDxXgZFM194hGPcOvgl7rS0YZ33tmccsEcx%0AEOREEKVUa%2BC9QSm4d8mZkfR0a4X2Y%2Ff9cqTq%2FB1Zg6Qtdub7qHe%2FLseB7wgf%0ArbcOLF2W%2Fk%2BKu7aj1PojRTqepdEZz5jzz7M%2Fv3%2FgifF5p6Hkm7jSCBP5YJAT%0A%2BO56a87ATQRYGx8wAQgA3DuabwlQPDinBY%2FvRpPzdFHjLdKCevhDVtwQSYWR%0A0M%2Bz5%2Fj45rd%2BDZXYd5u3Uw%2F%2BBPR6gtlLG7xtA7%2FDlMzPpa76C0hlhpX3lKuU%0AVenxIIeCwHqjJZs%2FcdG5ctAQRrc76bA%2FJ4McP8fN2138CdJhvWNObqExB69N%0AeZiLdWva6fVf%2F8uxRBpn1obpirvVyhMxioILHhJAHwiFfs94MwuQbxzmrddm%0A4hxN8Tmzvhg9inTe0l9QTnLJjXPq1iQOvpgB27ljnXPZG2VbA%2BUBaz1tOSAE%0ArPHVTKO%2FesowmwiDlpaNa0DoTKcvu88K%2BdU2PY%2FKzQmInVPJ%2F87ExSdfPwXe%0A%2FQARAQABwsBfBBgBCAATBQJYGx8wCRASu6xxhpMcXQIbDAAAL3gH%2F3A1XFGi%0Ak8lZhVnYRD9FwmQU5%2BKOnRABSumsM5Dicj4pnHhVmeokntLCQyE9dyNM6zjV%0AOvL6brjB4tBwkq71iv%2B41HrZOUy%2F8uHHDOb9MqZS6poFzeOeoAS0QT26RGe2%0A%2FV2Trx87Hn%2BEQX9AKK5UaxosiDneuU78v9xciBlsZN9GvHTV1t8BWlO%2BQKfe%0AKyWkpGMe5Dh9HjOol7WLJ1V7j3JODqIc7abip%2BGlvyKdxCQip4O9HqlgdxM8%0ABSu2FwPeHUTwiHr80zm0k8j2FYN7icBEx9xGUXzfnkapk1SWar2t%2F%2Bm58FXB%0AYwg3Zin3PFSv20AXQtbQ3oJEzpWUydMn53o%3D%0D%0A%3DyTt7%0D%0A-----END%20PGP%20PUBLIC%20KEY%20BLOCK-----%0D%0A%0D%0A"
	        //     },
	        //     orgKey1: {
	        //         private: "-----BEGIN%20PGP%20PRIVATE%20KEY%20BLOCK-----%0D%0AVersion%3A%20OpenPGP.js%20v2.3.3%0D%0AComment%3A%20http%3A%2F%2Fopenpgpjs.org%0D%0A%0D%0AxcMGBFgbH4YBCADPvpxmqGsMKIUJIWGvx4uROwYPJ%2B4w9R%2BS4%2FRgJ9JwdqfF%0A18adDCx%2BekEQXPSQoaWSTjBaq8CCDM3uBBfPiDoO5RW2stuQXw%2BcReWmkyfT%0AL2i6TR5717e43CZbj2IMH0kznFFhxaQOREUUNpmn5oeKVpeHIam8l520%2Bef1%0Af%2B9Kc%2Bznh0QaR2gVw9u9rCEvSz1aCLxwL5OGhYdZzbhEdER%2BhmiG1YD8uBKY%0ALsqAlV4%2Fe7FYuk%2BZhvwiJpCydZaVCiR8lYu%2F91rTWT9LcXfLsatxvbBcxx9F%0ASkLa6ROQUxRkSUSs5JejdJBNDVdkOz1LR6xUQe89MnjQX71yxhD5CCEHABEB%0AAAH%2BCQMI7zXp3LKAfGdgLIviiFIQrED%2FMCxv4Wyi8S%2BvyKmfoFdJ3ldM5re2%0AYRRXIJ0%2FhCjEQb6ABuz9jeri7Lrn9rV%2FwazHwMGZTayJ7OVYBQmHWZngi2Ht%0A%2F0gOod0YSPrPyUfZRkl88Sv5WE3nbJXn%2BmaxlQTd9jvUhZUvfdeZrbsBp4S6%0AWuaqaEF%2FWm97LzqrWWhxV8oZKjz1%2B%2FIvV2Eq0YU7ocmWP465ZxSkTHHsqUEY%0AFGqOWYJbUv4P2GEH%2B44Jb9cgrPm%2B9OHdTV0w7aONbNt4w36%2FKjGDbkfakqj%2F%0AKMsv2iD9SViLlaUNPDK1WCTd0jI0uhgwxzwGGIXdgQnrlfQbsuhvqCGEI6yd%0ADf6Vn7yBV%2B1348v4qFRm1065p%2BCtHjWFsa08qAFPLkUeYnOfuRBLL155gMUQ%0A%2F3maFX5Uh43yqjhUw6tu89OJxP6BIaqMnX0JAP05AAwKgTUb2dTZ9ZuUl9lF%0ABOzWC%2BMy6KDpHl9enwJE7IiFKtbJpDVcHFEMLzHs6GymJDt%2BMM5MA0gOjbfm%0ADjCThuVP8oeeAz1CG8sc6PSZzSLcEl2i0GjyOs81DOLU0X5CvBQaq5jvDLx%2F%0ATq1aYEocFz4my5L8fpownE%2BrNKZFyS9qckOAzsYpdAsw7o0rZ%2F%2BEFO1%2FKpD3%0AIToYWdtg6z1FfizAqrQ%2BXbLmahAccaokhPWvTpCeRNUEf938peXMvkCDPd1y%0AAKASsCXqNMBWSJWjaicOAHfMCj7K%2BwtK6wjt%2F2AhrpX5wBTHO5eHW4YiS5U2%0AMxE3267ZCYXl8R9y0zUfDUvOzWGi8PwvdICXevj7332sJV1ef4Wk%2BeIGqsJw%0A0WpcHfyfOwZDX7AHLDOk%2B9Pa4Nbx6uv9BziEucnR%2Bx6myS8orf5Y7AcwYJcN%0A2wum5rtjzQBfI1v5XBVijDFlz2ISAlqEzQMgPD7CwHUEEAEIACkFAlgbH4YG%0ACwkHCAMCCRDJbJt4fRG9SQQVCAIKAxYCAQIZAQIbAwIeAQAAbI4IAIIMYwqg%0A70z6XRDydtAgdRMrSO%2BBt%2Bxn6YsPDnVhrsF5gVp3k0ekM1bqHMUbTkbqYsIS%0AFxlPaC1p1gEYRjcBQcJ0eBXdC0navVO%2By%2BB3TkE2%2F8W8DoOPFfjAg7yRjI6W%0At6dbXokpuFmu0T5EvCA2plvbtfpYYS0kuKQzq3tGkDKJ1MGpE7zx1dvh%2FvNB%0AUuhgJKhhLDTrGKnBnoRkFoGJo%2BXYM1Xm0%2FrKNVTPq305%2BVySnw7PeLZcZkJU%0AQlOolkkqM3JYg%2Fbl5NXmehenwXHi8dqIYAhK6AqWBmnyruhxcANaXzt4xFzh%0Aara9d65zzvunc9E2ntSG63OyHiDjW2PT9XXHwwYEWBsfhgEIAMuZIDZG1U2u%0AgX6EfyM3Q0HAQZdy2rt6nLbFS6s02aYtD6pzfXEvsX561mWTtPaq%2FqdCQB9k%0A4qpkf3eRQh0Yke24WX9SRd6qME4UbptM4BftuC92hbFBlbzHL1e15%2FSEZpz9%0Ap4f0pjZMGdFU8SERmJYaz7b3uN7%2FrT8roxlRAHW4BXpHiurjBjL5kF3bOM10%0At%2FqQJ0Nyam7jO2Wh19VIgRm11XC6z6O198KJOPp5v6R%2BiB5epo3GBAQxe8sS%0AqKNJGilS%2Bq%2F4bohKhQztyFYp2P95A2sbSyPoe7hAF6qS5JBdCd8QvuGvl1fH%0Ajs4CoZublGQc2cGAyszBiE%2BdsGEiIDsAEQEAAf4JAwhgXi7%2FYSLD42BRXiVG%0AjeEYeghRSQP%2BX0D%2F4CcwtOqQ1DoRV0XJdfMo8xs4NRH4pBdcILVWZiyT0y4v%0AQeFD57bLe8hDM0kAa6PN5k1Vo2rGdFgZOUW84xwfMYRR%2BLg8Ca5YUlnh48zx%0ATtJqn1VlvjyrFhlNpcrvyjG4EWq56jh7oiqJ17LO%2BtgUkdwvmBSff7N8L64T%0AimNu%2BoxR9%2B2aduIY%2BsRFWAjIQH0P12Ji3mQUbd2KjBsM%2B0cZ3vdeEM12Q82J%0ASCP%2F987OawvyZZs8gjscJNRBOtfvmn0jdBcZVfLdBASFN4FVtbmy%2BIhquknu%0AMc3Gx%2BP0T0dOl07ghuO0pWom9l6F9GtzARzRKLMlxFzSvi%2FHBZbCAPDxgV0S%0AK3lbp8Tm7VdJTUSDyQpOBcJTJR1bfiYLHQTMQr%2BmIuFT0bmcjbP9QqoNennJ%0Aa%2Ftji2D1VY5tTnvm6krjZ90kxMNZiT54N2K5%2BQBCJbXKD%2BU9KDmtlPbP1pFt%0ARytUR0eEHxRIw%2BnQFY6AYKSATPPjLvIfhyOnYec%2B6h4XgEu6X42VBUfHj3Wk%0AD75h9K5S7cRoe2wERywxcb2SeNF6s6BELH337MA13RRdhFVTzo1RR4zUBAR%2F%0AMEyi38nWIi%2F3hHuu0BX9HCPXhoCruPaLxZTomNJOMkfEz5zH4ZFD%2F13rIS%2Bd%0ACRl2%2FIhlFmNJqZgSevafatuVAJhpa6Zm%2FK4KiRu0%2Fwq3x5HZcFUDx7FKZ4a8%0A9GLTdSgD%2BzOU%2FC%2Fv8jJhPT7rzo5eZGZRUFWH%2FH1F2RceGlzIs0glxeAfqQx7%0ATAgQ%2FeU9c6%2F8PvL%2B2275cvrrvscgOd8knwzsrj3ufuQSgz2Pf%2BXBa78dNaVr%0AS5q%2FuxWwbYv1uvC2iJsodPEt9iybIK8ZJXGVzo4B6hbC3NUgLWeLHXTtsX1O%0At6v5r0zCwF8EGAEIABMFAlgbH4cJEMlsm3h9Eb1JAhsMAAAi3gf%2BOffxnmpt%0AW7NBBwYS93vYbdXBagi4MFDH23ClG1qvpNbfcvkbYqry7mvLe%2FfAfFRjynTY%0Aexi1GYK1s8a1V%2BAsjnd36IuH6FHnjRpC%2FcEz89EvYrkRFEp2wmMwXrWW2yev%0Aeb3xGqvO1WQQl1DpMBVgKx3EeskLt%2FBNL8lBEGHHmwcR6K7URQZjSJE7q3LA%0AsBa7R5Wd10iWWqsftYlFx7jzljvQ2OHw%2B%2BT%2BvjkZgI6xRh4G7AS3bamxL6bO%0AXSBwerJI1%2BAwlCMyvy%2Bii7EMYoyr%2BikfEtKTo204vZsDGuXUozKm21V%2BIF%2BA%0Ak3OzCjbgb170Yui3%2BOp3zIPYQSuqHzu8AQ%3D%3D%0D%0A%3D7kXf%0D%0A-----END%20PGP%20PRIVATE%20KEY%20BLOCK-----%0D%0A",
	        //         public: "-----BEGIN%20PGP%20PUBLIC%20KEY%20BLOCK-----%0D%0AVersion%3A%20OpenPGP.js%20v2.3.3%0D%0AComment%3A%20http%3A%2F%2Fopenpgpjs.org%0D%0A%0D%0AxsBNBFgbH4YBCADPvpxmqGsMKIUJIWGvx4uROwYPJ%2B4w9R%2BS4%2FRgJ9JwdqfF%0A18adDCx%2BekEQXPSQoaWSTjBaq8CCDM3uBBfPiDoO5RW2stuQXw%2BcReWmkyfT%0AL2i6TR5717e43CZbj2IMH0kznFFhxaQOREUUNpmn5oeKVpeHIam8l520%2Bef1%0Af%2B9Kc%2Bznh0QaR2gVw9u9rCEvSz1aCLxwL5OGhYdZzbhEdER%2BhmiG1YD8uBKY%0ALsqAlV4%2Fe7FYuk%2BZhvwiJpCydZaVCiR8lYu%2F91rTWT9LcXfLsatxvbBcxx9F%0ASkLa6ROQUxRkSUSs5JejdJBNDVdkOz1LR6xUQe89MnjQX71yxhD5CCEHABEB%0AAAHNAyA8PsLAdQQQAQgAKQUCWBsfhgYLCQcIAwIJEMlsm3h9Eb1JBBUIAgoD%0AFgIBAhkBAhsDAh4BAABsjggAggxjCqDvTPpdEPJ20CB1EytI74G37Gfpiw8O%0AdWGuwXmBWneTR6QzVuocxRtORupiwhIXGU9oLWnWARhGNwFBwnR4Fd0LSdq9%0AU77L4HdOQTb%2FxbwOg48V%2BMCDvJGMjpa3p1teiSm4Wa7RPkS8IDamW9u1%2Blhh%0ALSS4pDOre0aQMonUwakTvPHV2%2BH%2B80FS6GAkqGEsNOsYqcGehGQWgYmj5dgz%0AVebT%2Bso1VM%2BrfTn5XJKfDs94tlxmQlRCU6iWSSozcliD9uXk1eZ6F6fBceLx%0A2ohgCEroCpYGafKu6HFwA1pfO3jEXOFqtr13rnPO%2B6dz0Tae1Ibrc7IeIONb%0AY9P1dc7ATQRYGx%2BGAQgAy5kgNkbVTa6BfoR%2FIzdDQcBBl3Lau3qctsVLqzTZ%0Api0PqnN9cS%2BxfnrWZZO09qr%2Bp0JAH2TiqmR%2Fd5FCHRiR7bhZf1JF3qowThRu%0Am0zgF%2B24L3aFsUGVvMcvV7Xn9IRmnP2nh%2FSmNkwZ0VTxIRGYlhrPtve43v%2Bt%0APyujGVEAdbgFekeK6uMGMvmQXds4zXS3%2BpAnQ3JqbuM7ZaHX1UiBGbXVcLrP%0Ao7X3wok4%2Bnm%2FpH6IHl6mjcYEBDF7yxKoo0kaKVL6r%2FhuiEqFDO3IVinY%2F3kD%0AaxtLI%2Bh7uEAXqpLkkF0J3xC%2B4a%2BXV8eOzgKhm5uUZBzZwYDKzMGIT52wYSIg%0AOwARAQABwsBfBBgBCAATBQJYGx%2BHCRDJbJt4fRG9SQIbDAAAIt4H%2Fjn38Z5q%0AbVuzQQcGEvd72G3VwWoIuDBQx9twpRtar6TW33L5G2Kq8u5ry3v3wHxUY8p0%0A2HsYtRmCtbPGtVfgLI53d%2BiLh%2BhR540aQv3BM%2FPRL2K5ERRKdsJjMF61ltsn%0Ar3m98RqrztVkEJdQ6TAVYCsdxHrJC7fwTS%2FJQRBhx5sHEeiu1EUGY0iRO6ty%0AwLAWu0eVnddIllqrH7WJRce485Y70Njh8Pvk%2Fr45GYCOsUYeBuwEt22psS%2Bm%0Azl0gcHqySNfgMJQjMr8voouxDGKMq%2FopHxLSk6NtOL2bAxrl1KMypttVfiBf%0AgJNzswo24G9e9GLot%2Fjqd8yD2EErqh87vAE%3D%0D%0A%3Dy9at%0D%0A-----END%20PGP%20PUBLIC%20KEY%20BLOCK-----%0D%0A%0D%0A"
	        //     },
	        //     orgKey2: {
	        //         private: "-----BEGIN%20PGP%20PRIVATE%20KEY%20BLOCK-----%0D%0AVersion%3A%20OpenPGP.js%20v2.3.3%0D%0AComment%3A%20http%3A%2F%2Fopenpgpjs.org%0D%0A%0D%0AxcMGBFgbIA8BCAC%2Bj0TdoQAU4dBODVS6gSDjoemob2yqcuGET%2F7VqOujJBO2%0A3Kj2jI7piAkdzryYOuD6C2R7ZPctAUG2bUoF7p9jBtr%2FIc3C7O6s4NEcIC22%0Agmzwi8zLn96ozgTCWqmsu7iYDaHR0JE5fi19h6Xb3eMEELPLZfgZpaEaqSRd%0A4QsPLOj2GRmrrJ98ZTb7jY3u0%2B%2B68Dosdp4FUCiRSM67Kmc1%2B2mVrOqhKZhw%0AAKvQOe8MfUBVk7UiyMG3yOMjcbmFobfuiRCIV24m0iCto%2BG%2F2j2m%2BMbnLJ4W%0AMttCs6ycGXgfkJdp0bUXpcBAXQToHLqgRw6QrGqCT5aw0vstRTPwer95ABEB%0AAAH%2BCQMImG8HCd2yV0BgzFfYj8nB93v4GJ%2Bti31y4WK8CJfKqhqC0EIHLJS8%0Ad36NA%2BoJpcIwgBqXwuamO238qdp%2F18VWUHU5cERDhoAXY100SBIHRBf6HXA%2F%0AjNw6GSCvXaNeYCEtFsizIfloTRMzSXQmE%2FGZYyM2LJvJEDpR%2FHm1kq%2B293DC%0AkCPvhhBpMORyy680iw6Yquq5XYhud9%2FkkDHUxqrAO2WqQ94P0LQWwTuGVUKV%0ANSc%2FcrxJyG1Q7i3p2RV5%2FudzxeNygCQ5XZdQIh6LKnFYp9SvJ3oiK2xh61mv%0AObXVjchP9pkOwnVlSQJ6IxYpPbkA6Ius5b97DrenraiKm0MnrV8BfVvyCW6H%0AiQtsQCcwMfICBX8WGdlWd4nABxlnnqm%2F7c%2F9X%2BOlQD8%2BSM3q%2BMiG%2F%2FazClA7%0ADJT8L7SiqRVBG8V26I09RVGkxsGksiMEouTJ1fP7f9v%2BnOqK8apeTQzuxTS8%0AAe%2FLy%2B28cBHIHr33rTRBfk3ekX0omQ1xQ2RhK2rD1QPUVpsL%2FEKNskPT2n2j%0AAD3F9koGgOTbEajy%2BEi0lMreqNLTrdHMw5joh7UB8xtuWNrX116wxrjHsTpq%0A1muyytqNCiM5VvwRNziZfbXldSyUQorD%2BoZqmOdmJucSHhV4HILASiMrIqyL%0A6dWNLWJS3C6KBrlA%2Fuk4Sa28fKgeWsaQx1yY3r%2BkeCq0PWLaPNDNrt%2FNn4i2%0A%2FTelccQ5i3O0Ks8B5rTZHcsbivtIZHQmELg7nOyZZTfy9a4h2ne3jSNBDhLw%0AY%2BxOoeBOMSe5qBvYJjXixCSMZjsMsVYEn%2FxAdf3ytK0fdydWq75azunSf0Xv%0Az3YqxjbH%2FeFQaCt44BfCT2JI0gerZagrO%2FQ4VPiqNkN4pgXR5SSrnbXpxW%2FF%0AjKkzc9gTxzJQjo2TrXx3%2FJeO7cZg%2BjKEzQMgPD7CwHUEEAEIACkFAlgbIA8G%0ACwkHCAMCCRB03IKznQZqJQQVCAIKAxYCAQIZAQIbAwIeAQAA2YkH%2F1vVQZj6%0Aj2MDhkBC6mJME8VWmGMorjcIh3tk%2FxddN2xp8LHsd6Jclc59hS1RWyC%2FMHyg%0ASmxtnvh0h%2BI22e7%2FmipgcL%2FadaJSG4rLEPh0s4%2FcAzWm2aox7FnWuMwvzkxV%0ALPByFNiGbR4KviMYfdAXpOOYbePxg1QVv4o%2FnaHUu042b0%2FlgtPDMhOC7TIy%0ArgRuKOE4VhBqUtA%2FvVDGzId%2FvuNuyK4IGSvm7WfuoIXcFLn4AynHP%2B0drQaE%0AzJsrZEJxSdAY5ZB8PwizBzrKtyhtA2EsSpoIoNKfbDG7GenrZ0jrXlDDp%2B0r%0AdT7x6IDKutTy8TyaxY9nEeIRw4L7894ybQXHwwYEWBsgDwEIAMVfLQBd5JIe%0AwGcJYWp%2BAUMOL7%2BVRWrsyY4efx9UT8KbFtutROrz1xF6j98A17CX%2BaD4DnK9%0AtVSBg065q6gSA0lI%2F2OCxMDykjABKlzHTw0hyG4gIXGYL1U2VHItNcmF2iMo%0AeedFHR%2F3X7706WpLFKVgbNSlGpsaHcWV8t6s8MvaUefTmJeVcYQXXkVnRjvb%0AMvo4KmVRjoACppRSqB2TYSiB2HdsBRqIs%2FuiYymzAfUulEWH6ln7GErm7IER%0AszvZ8wH%2FGK1KrgaBpNrVCAFggBf0oQbtJ6gWbaJfSb8P%2FmEsuUc49zbeaT6m%0AigbayaIRBsOBQyYKpzmrc8kpOw6H4VMAEQEAAf4JAwihlphllWU4%2BmC8UQIl%0AUl2JjZW85rE64GMK4eu%2B%2B9O%2FIx%2FxQrJPZEeh36ukDcuaKbRcSKLbhEoQzISE%0AJ9e2NwHenvUOLWIPegjcysR1Pr2yvWpKOajZQjelY8xvYaJk7Azlt0mMiCyB%0A2jICdIlUAkY9tFG29DN1v65GXLk2iWTNLkpU4JwcjHWUVzJ8lVyD6ftgUuWM%0AJX80U11s4ARmTpfRwZTNyvjc9NLLOYzInFaZ9r2ZhWVVG6GHVFI1Ia7v49fy%0AiHeOUtLCV41fffPYRzukWMsNr8wSaO56XzKaFMK8XudnendwwawgCcY4LQoY%0ANyg8ryhTR5aurV%2BB6meJRwR9ugyYN5MwRsnLbsKqrMxe3LLlDeJB2D%2BRzjS1%0A%2Bs3Y%2FG9pUPQ1Ci05avDACqKZB3yXN56NnonR%2FwXyZAJDRCfj%2F78EAI3E526C%0Ak6cSt0jJM%2BcVikNpB9xBBExc7Aa4slwIGiLEm%2B5q5C3%2BgGaoV1RnckjfXZpg%0AhxV5OpkPhN8XcimJIQ8JhWc37KDolX0f4HHvRMwtf%2F4OmH2wxB%2FxfAkQKXLs%0Axlro9ibaRp6J%2BlrfRzcIkeoqi4D3XndU7CdsSLyWKvs0OaNPGb2HZ2PyshpS%0A0ujP23GlioK89Ot7KC7IBtVCaDWsFGKtQKctfXF2sv0RGlCFTaDPu8ywqPRd%0Ars8IrePP%2BuH1yhciCd34Z8lAnUydDCQI3Nr7ELaCdYvP1Vy2gQ4KXpld35bv%0Ao%2Fg7%2FC8HPIadpB7G%2F4kvrNwWosoLFncXV8FSScKVPeO%2BdFlLcLZpdFBOma20%0AwywSCs1DbyjUGnhVGrazsEqw7uD6n91DCeT4Z47yoacO9%2BOzfzYK82BJsnmR%0AvYCJC7neWmWgfm9Rg6U%2BLlw68%2FTOFUaaOik2gr6Ftoc2I4vPIKc7rx8JXgWg%0Ac8epl37CwF8EGAEIABMFAlgbIBAJEHTcgrOdBmolAhsMAADqMQf9HOfAdhra%0ATRGGf83GNuyry8ui4fLHjQGOT%2FVyMyA56LmZ5xsObQEnTjJpNKSqOkPJlGmX%0A3RI8%2BeUpPzAvkiqsQIJujXoGn86CK4zmOHm2VTeQ%2BrwShy4MLOHHJbCEHPOp%0AZ8r8zmvihEqk5rkaoUYNE8k%2B2JY8Rv8cjZN79jYKnd47oTsc4uugOoJ0tqm6%0ARdR1kKDpdBFtZpAQlbWIVv%2BvpxzyVBSy1YEMZEgT5qSVlVX3ImhUBDUZRtnx%0AUbGoxnJd49Ds0nIk5NF9u4qG%2FlS41z9QnwkSCb0v7oF6dPMTQyf9A4qhy8f%2F%0ABIh7WeFXiZ%2B8h2%2BP0PPTo9ADGtea%2Byjj7g%3D%3D%0D%0A%3DI7ED%0D%0A-----END%20PGP%20PRIVATE%20KEY%20BLOCK-----%0D%0A",
	        //         public: "-----BEGIN%20PGP%20PUBLIC%20KEY%20BLOCK-----%0D%0AVersion%3A%20OpenPGP.js%20v2.3.3%0D%0AComment%3A%20http%3A%2F%2Fopenpgpjs.org%0D%0A%0D%0AxsBNBFgbIA8BCAC%2Bj0TdoQAU4dBODVS6gSDjoemob2yqcuGET%2F7VqOujJBO2%0A3Kj2jI7piAkdzryYOuD6C2R7ZPctAUG2bUoF7p9jBtr%2FIc3C7O6s4NEcIC22%0Agmzwi8zLn96ozgTCWqmsu7iYDaHR0JE5fi19h6Xb3eMEELPLZfgZpaEaqSRd%0A4QsPLOj2GRmrrJ98ZTb7jY3u0%2B%2B68Dosdp4FUCiRSM67Kmc1%2B2mVrOqhKZhw%0AAKvQOe8MfUBVk7UiyMG3yOMjcbmFobfuiRCIV24m0iCto%2BG%2F2j2m%2BMbnLJ4W%0AMttCs6ycGXgfkJdp0bUXpcBAXQToHLqgRw6QrGqCT5aw0vstRTPwer95ABEB%0AAAHNAyA8PsLAdQQQAQgAKQUCWBsgDwYLCQcIAwIJEHTcgrOdBmolBBUIAgoD%0AFgIBAhkBAhsDAh4BAADZiQf%2FW9VBmPqPYwOGQELqYkwTxVaYYyiuNwiHe2T%2F%0AF103bGnwsex3olyVzn2FLVFbIL8wfKBKbG2e%2BHSH4jbZ7v%2BaKmBwv9p1olIb%0AissQ%2BHSzj9wDNabZqjHsWda4zC%2FOTFUs8HIU2IZtHgq%2BIxh90Bek45ht4%2FGD%0AVBW%2Fij%2BdodS7TjZvT%2BWC08MyE4LtMjKuBG4o4ThWEGpS0D%2B9UMbMh3%2B%2B427I%0ArggZK%2BbtZ%2B6ghdwUufgDKcc%2F7R2tBoTMmytkQnFJ0BjlkHw%2FCLMHOsq3KG0D%0AYSxKmgig0p9sMbsZ6etnSOteUMOn7St1PvHogMq61PLxPJrFj2cR4hHDgvvz%0A3jJtBc7ATQRYGyAPAQgAxV8tAF3kkh7AZwlhan4BQw4vv5VFauzJjh5%2FH1RP%0AwpsW261E6vPXEXqP3wDXsJf5oPgOcr21VIGDTrmrqBIDSUj%2FY4LEwPKSMAEq%0AXMdPDSHIbiAhcZgvVTZUci01yYXaIyh550UdH%2FdfvvTpaksUpWBs1KUamxod%0AxZXy3qzwy9pR59OYl5VxhBdeRWdGO9sy%2BjgqZVGOgAKmlFKoHZNhKIHYd2wF%0AGoiz%2B6JjKbMB9S6URYfqWfsYSubsgRGzO9nzAf8YrUquBoGk2tUIAWCAF%2FSh%0ABu0nqBZtol9Jvw%2F%2BYSy5Rzj3Nt5pPqaKBtrJohEGw4FDJgqnOatzySk7Dofh%0AUwARAQABwsBfBBgBCAATBQJYGyAQCRB03IKznQZqJQIbDAAA6jEH%2FRznwHYa%0A2k0Rhn%2FNxjbsq8vLouHyx40Bjk%2F1cjMgOei5mecbDm0BJ04yaTSkqjpDyZRp%0Al90SPPnlKT8wL5IqrECCbo16Bp%2FOgiuM5jh5tlU3kPq8EocuDCzhxyWwhBzz%0AqWfK%2FM5r4oRKpOa5GqFGDRPJPtiWPEb%2FHI2Te%2FY2Cp3eO6E7HOLroDqCdLap%0AukXUdZCg6XQRbWaQEJW1iFb%2Fr6cc8lQUstWBDGRIE%2BaklZVV9yJoVAQ1GUbZ%0A8VGxqMZyXePQ7NJyJOTRfbuKhv5UuNc%2FUJ8JEgm9L%2B6BenTzE0Mn%2FQOKocvH%0A%2FwSIe1nhV4mfvIdvj9Dz06PQAxrXmvso4%2B4%3D%0D%0A%3DE9fI%0D%0A-----END%20PGP%20PUBLIC%20KEY%20BLOCK-----%0D%0A%0D%0A"
	        //     }
	        // };


	        /* TEMP */
	        // if(this.state['encrypt']){
	        //     keys = {
	        //         userKey: 'key1',
	        //         orgKey1: 'key2',
	        //         orgKey2: 'key3'
	        //     };
	        // }else{
	        //     if(DECRYPT_STATE=='user'){
	        //         //если хотим расщифровать ключом пользователя
	        //         keys = {
	        //             userKey: 'key1',
	        //         }
	        //     }else{
	        //         //если хотим расшифровать ключами организаций
	        //         keys = {
	        //             orgKey1: 'key2',
	        //             orgKey2: 'key3'
	        //         };
	        //     }
	        // }
	        return keys;
	    };

	    return Keys;
	}();

	exports.Keys = Keys;

/***/ }
/******/ ]);